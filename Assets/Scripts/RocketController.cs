﻿using UnityEngine;
using System.Collections;

public class RocketController : MonoBehaviour{
	public float thrust = 0.05f;
	public float turn = 0.5f;
	public Rigidbody rigidbody;
	public AudioSource audio;

	private Animator animator;
	private Light flameLight;
	private ParticleSystem smoke;
	private ParticleSystem jetFlame;
	private float limitVelocity= 10f;

	void Awake(){
		animator = GetComponent<Animator> ();
		flameLight = GetComponentInChildren <Light> ();
		ParticleSystem[] particleSystem = GetComponentsInChildren <ParticleSystem> ();
		smoke = particleSystem[0];
		jetFlame = particleSystem[1];
	}

	void FixedUpdate () {
		if (Input.GetKey (KeyCode.Space)) {
			rigidbody.AddRelativeForce (Vector3.up * thrust, ForceMode.Acceleration);
			if(rigidbody.velocity.magnitude > limitVelocity){
				rigidbody.velocity = Vector3.ClampMagnitude(rigidbody.velocity, limitVelocity);
			}
			/*if (Input.GetKey (KeyCode.LeftArrow)) {
				animator.SetFloat("RudderN",-2,0.5f,2 * Time.deltaTime);
				animator.SetFloat("RudderS",-2,0.5f,2 * Time.deltaTime);
				rigidbody.AddForce( 0, 0, (turn * limitVelocity));
			}
			else if (Input.GetKey (KeyCode.RightArrow)) {
				animator.SetFloat("RudderN",1,0.5f,2 * Time.deltaTime);
				animator.SetFloat("RudderS",1,0.5f,2 * Time.deltaTime);
				rigidbody.AddRelativeTorque( 0, 0, (-turn * limitVelocity));
			}
			else{
				animator.SetFloat("RudderN",0,0.5f,2 * Time.deltaTime);
				animator.SetFloat("RudderS",0,0.5f,2 * Time.deltaTime);
			}*/
			if (Input.GetKey (KeyCode.UpArrow)) {
				animator.SetFloat("RudderE",-1,0.5f,2 * Time.deltaTime);
				animator.SetFloat("RudderW",-1,0.5f,2 * Time.deltaTime);
				rigidbody.AddRelativeTorque( (turn * limitVelocity), 0, 0);
				rigidbody.AddRelativeForce (Vector3.forward * thrust, ForceMode.Acceleration);
			}
			else if (Input.GetKey (KeyCode.DownArrow)) {
				animator.SetFloat("RudderE",1,0.5f,2 * Time.deltaTime);
				animator.SetFloat("RudderW",1,0.5f,2 * Time.deltaTime);
				rigidbody.AddRelativeTorque( (-turn * limitVelocity), 0, 0);
				rigidbody.AddRelativeForce (Vector3.back * thrust, ForceMode.Acceleration);
			}
			else{
				animator.SetFloat("RudderE",0,0.5f,2 * Time.deltaTime);
				animator.SetFloat("RudderW",0,0.5f,2 * Time.deltaTime);
			}
			smoke.enableEmission = true;
			jetFlame.enableEmission = true;
			audio.volume =  Mathf.Lerp(audio.volume, 1, (5 * Time.deltaTime));
			if(!audio.isPlaying){
				audio.Play();
			}
			flameLight.intensity = Mathf.Lerp(flameLight.intensity, 8, 10 * Time.deltaTime);
		} 

		else{
			flameLight.intensity = Random.Range(0.5f,2);
			smoke.enableEmission = false;
			jetFlame.enableEmission = false;
			audio.volume = Mathf.Lerp(audio.volume, 0, (5 * Time.deltaTime));
		}
		float velocity = Mathf.Clamp(rigidbody.velocity.magnitude, 0, 1);
		}
	}
