﻿using UnityEngine;
using System.Collections;

public class LevelController : MonoBehaviour {

	private float smooth = 2;
	private bool guiVisible = true;

	void Update () {		
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.LoadLevel(Application.loadedLevel);
		}
		if (Input.GetKeyDown (KeyCode.H)) {
			guiVisible = guiVisible == true ? false : true;
		}
	}
	
	void OnGUI() {
		if(guiVisible){
			GUI.Label (new Rect (10, 5, 150, 100), "Rocket Controls:");
			GUI.Label (new Rect (120, 5, 160, 100), "Move Rocket: Up and Down Arrows");	
			GUI.Label (new Rect (120, 35, 160, 100), "Rotate Camera: Left & Right Arrows");
			GUI.Label (new Rect (10, 70, 150, 100), "Thrust: Space");
			GUI.Label (new Rect (10, 90, 150, 100), "Restart: Escape");
			GUI.Label (new Rect (10, 110, 150, 100), "Hide GUI: H");
		}
	}
}
