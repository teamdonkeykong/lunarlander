﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crash : MonoBehaviour {
	private Rigidbody rigidBody;
	private AudioSource audioSource;
	public AudioClip crashsound;
	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void onCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "Player") {
			audioSource = GetComponent <AudioSource> ();
			audioSource.clip = crashsound;
			audioSource.Play ();
		}
	}
}
